<?php

include 'route.php';
include 'src/about.php';
include 'src/home.php';
include 'src/contact.php';

$route = new Route();

$route->add('/', function() {
	echo 'Hey this is home';
});

$route->add('/about', 'About');
$route->add('/contact', 'Contact');

echo '<pre>';
print_r($route);

$route->submit();